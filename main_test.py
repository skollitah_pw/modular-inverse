import unittest

import main


class TestModularInverse(unittest.TestCase):

    def test_modular_inverse(self):
        self.assertEqual(main.modular_inverse(8, 17), 15)
        self.assertEqual(main.modular_inverse(8, 13), 5)
        self.assertEqual(main.modular_inverse(21, 25), 6)
        self.assertEqual(main.modular_inverse(3, 7), 5)


if __name__ == '__main__':
    unittest.main()
