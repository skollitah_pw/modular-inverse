def modular_inverse_simple(a, z):
    for i in range(z):
        inv_a = a * i % z
        if inv_a == 1:
            return i

    raise Exception('Result not found')


def e_gcd(a, b):
    if a == 0:
        return b, 0, 1
    else:
        gcd, x, y = e_gcd(b % a, a)
        return gcd, y - (b // a) * x, x


def modular_inverse(a, z):
    gcd, x, y = e_gcd(a, z)
    return x % z


if __name__ == '__main__':
    try:
        a = int(input('a: '))
        z = int(input('z: '))
        print(f'Modular inverse of {a} in group {z} is {modular_inverse(a, z)}')
    except Exception as e:
        raise e
